#include <mpi.h>

#include <cassert>
#include <fstream>
#include <iostream>
#include <random>
#include <vector>

#include "MMM.hpp"

int main() {
  MPI_Init(nullptr, nullptr);

  int rank, P;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &P);

  if (rank == 0) {
    std::cout << "N,t_naive,t_cyclic" << std::endl;
  }

  /// Initialising matrices A and B
  for (std::size_t N = 8; N <= 8; N <<= 1) {
    std::vector<double> A(N * N), B(N * N), C(N * N, 0.);

    if (rank == 0) {
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_int_distribution<> dis(-1., 1.);
      double num = 0;
      for (std::size_t i = 0; i < N; ++i) {
        for (std::size_t j = 0; j < N; ++j) {
          A[i * N + j] = num; /*dis(gen)*/
          ;
          B[i * N + j] = num++; /*dis(gen)*/
          ;
        }
      }

      // DEBUG print A and B
      std::cout << "A: \n";
      for (std::size_t i = 0; i < N; ++i) {
        for (std::size_t j = 0; j < N; ++j) {
          std::cout << A[i * N + j] << " ";
        }
        std::cout << std::endl;
      }
      std::cout << std::endl;

      std::cout << "B: \n";
      for (std::size_t i = 0; i < N; ++i) {
        for (std::size_t j = 0; j < N; ++j) {
          std::cout << B[i * N + j] << " ";
        }
        std::cout << std::endl;
      }
      std::cout << std::endl;
    }

    /// Broadcast A and B to other processors
    //    MPI_Bcast(A.data(), N * N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    //    MPI_Bcast(B.data(), N * N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    /// Start Benchmark
    double time_cyclic = 0., time_naive = 0.;

    for (std::size_t i = 0; i < 1; ++i) {
      double t1, t2;

      if (rank == 0) {
        t1 = MPI_Wtime();
      }
      //      MMM(A, B, C, N);
      matrix_t Aij, Bij, Cij(N * N / P, 0.);
      distributeMatrixTo2Dgrid(A, Aij, std::sqrt(P));
      distributeMatrixTo2Dgrid(B, Bij, std::sqrt(P));

      // DEBUG print Aij and Bij of rank 1
      MPI_Barrier(MPI_COMM_WORLD);
      if (rank == 1) {
        std::ofstream fout;
        fout.open("out1.dat");
        fout << "Aij: \n";
        for (std::size_t r = 0; r < std::sqrt(Aij.size()); ++r) {
          for (std::size_t s = 0; s < std::sqrt(Aij.size()); ++s) {
            fout << Aij[r * std::sqrt(Aij.size()) + s] << " ";
          }
          fout << std::endl;
        }
        fout << std::endl;

        fout << "Bij: , size " << std::sqrt(Aij.size()) << std::endl;
        for (std::size_t r = 0; r < std::sqrt(Aij.size()); ++r) {
          for (std::size_t s = 0; s < std::sqrt(Aij.size()); ++s) {
            fout << Bij[r * std::sqrt(Aij.size()) + s] << " ";
          }
          fout << std::endl;
        }
        fout << std::endl;
        fout.close();
      }

      MMM25d(Aij, Bij, Cij, N, 1);
      reduce2DgridToMatrix(Cij, C, std::sqrt(P));
      if (rank == 0) {
        t2 = MPI_Wtime();
        time_cyclic += t2 - t1;
      }
    }

    /// TEST
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
      std::cout << "2.5D MMM: C: \n";
      for (std::size_t i = 0; i < N; ++i) {
        for (std::size_t j = 0; j < N; ++j) {
          std::cout << C[i * N + j] << " ";
        }
        std::cout << std::endl;
      }
      std::cout << std::endl;

      time_cyclic /= 10;
      std::vector<double> C2(N * N, 0.);
      for (std::size_t l = 0; l < 10; ++l) {
        double t1, t2;
        t1 = MPI_Wtime();
        C2 = std::vector<double>(N * N, 0.);
        naive_MMM(A, B, C2, N);
        t2 = MPI_Wtime();
        time_naive += t2 - t1;
      }

      // print result C
      std::cout << "C_naive: \n";
      for (std::size_t i = 0; i < N; ++i) {
        for (std::size_t j = 0; j < N; ++j) {
          std::cout << C2[i * N + j] << " ";
        }
        std::cout << std::endl;
      }

      // check correctness
      time_naive /= 10;
      std::cout << N << "," << time_naive << "," << time_cyclic << std::endl;
      for (std::size_t i = 0; i < N; ++i) {
        for (std::size_t j = 0; j < N; ++j) {
          assert(std::abs(C[i * N + j] - C2[i * N + j]) < 1e-10);
        }
      }
    }
  }

  MPI_Finalize();
}
