#pragma once

#include <tuple>
#include <vector>

using matrix_t = std::vector<double>;

void transpose(matrix_t& A, const std::size_t N);

void naive_MMM(const matrix_t& A, const matrix_t& B, matrix_t& C,
               const std::size_t N);

void distributeMatrixTo2Dgrid(const matrix_t& A, matrix_t& Aij,
                              const std::size_t grid_size);

void reduce2DgridToMatrix(const matrix_t& Cij, matrix_t& C,
                          const std::size_t grid_size);

inline std::tuple<std::size_t, std::size_t, std::size_t> getProcessorIndices(
    const std::size_t rank, const std::size_t P, const std::size_t c);
inline int getProcessorRank(
    const std::tuple<std::size_t, std::size_t, std::size_t> indices,
    const std::size_t P, const std::size_t c);
void MMM25d(matrix_t& Aij, matrix_t& Bij, matrix_t& Cij, const std::size_t N,
            const std::size_t c);

void MMM(const matrix_t& A, const matrix_t& B, matrix_t& C, const size_t M);
