#include <mpi.h>

#include <cassert>
#include <iostream>
#include <random>
#include <vector>

#include "MMM.hpp"

int main() {
  MPI_Init(nullptr, nullptr);

  int rank, P;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &P);

  if (rank == 0) {
    std::cout << "N,t_naive,t_cyclic" << std::endl;
  }

  /// MAIN LOOP
  for (std::size_t N = 8; N <= 1024; N <<= 1) {
    std::vector<double> A(N * N), B(N * N), C(N * N, 0.);

    if (rank == 0) {
      /// Initialising matrices A and B

      // std::random_device rd;
      // std::mt19937 gen(rd());
      // std::uniform_int_distribution<> dis(-1., 1.);
      double num = 0;
      for (std::size_t i = 0; i < N; ++i) {
        for (std::size_t j = 0; j < N; ++j) {
          A[i * N + j] = num; /*dis(gen)*/
          ;
          B[j * N + i] = num++; /*dis(gen)*/
          ;
        }
      }
      /*
                              //DEBUG print A and B
                              std::cout << "A: \n";
            for (std::size_t i = 0; i < N; ++i) {
              for (std::size_t j = 0; j < N; ++j) {
                std::cout << A[i * N + j] << " ";
              }
              std::cout << std::endl;
            }
            std::cout << std::endl;

                              std::cout << "B: \n";
            for (std::size_t i = 0; i < N; ++i) {
              for (std::size_t j = 0; j < N; ++j) {
                std::cout << B[i * N + j] << " ";
              }
              std::cout << std::endl;
            }
            std::cout << std::endl;
      */
    }

    /// Broadcast A and B to other processors
    MPI_Bcast(A.data(), N * N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(B.data(), N * N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    /// Start Benchmark
    double time_cyclic = 0., time_naive = 0.;

    for (std::size_t i = 0; i < 1; ++i) {
      double t1, t2;

      if (rank == 0) {
        t1 = MPI_Wtime();
      }
      // Call MMM
      MMM(A, B, C, N);

      if (rank == 0) {
        t2 = MPI_Wtime();
        time_cyclic += t2 - t1;
      }
    }

    /// TEST
    if (rank == 0) {
      /*			std::cout << "C_MMM: \n";
            for (std::size_t i = 0; i < N; ++i) {
              for (std::size_t j = 0; j < N; ++j) {
                std::cout << C[i * N + j] << " ";
              }
              std::cout << std::endl;
            }
            std::cout << std::endl;
      */
      time_cyclic /= 10;

      // Calculate maive C
      std::vector<double> C2(N * N, 0.);
      for (std::size_t l = 0; l < 10; ++l) {
        double t1, t2;
        t1 = MPI_Wtime();
        C2 = std::vector<double>(N * N, 0.);
        naive_MMM(A, B, C2, N);
        t2 = MPI_Wtime();
        time_naive += t2 - t1;
      }
      /*
                              //print result C
                              std::cout << "C_naive: \n";
            for (std::size_t i = 0; i < N; ++i) {
              for (std::size_t j = 0; j < N; ++j) {
                std::cout << C2[i * N + j] << " ";
              }
              std::cout << std::endl;
            }
                  std::cout << std::endl;
      */

      // check correctness
      time_naive /= 10;
      std::cout << N << "," << time_naive << "," << time_cyclic << std::endl
                << std::endl;
      bool failure = 0;
      for (std::size_t i = 0; i < N; ++i) {
        for (std::size_t j = 0; j < N; ++j) {
          if (!failure && std::abs(C[i * N + j] - C2[i * N + j]) > 1e-10) {
            std::cout << "INCORRECT RESULT at " << N << "  \n";
            failure = 1;
          }
          // assert(std::abs(C[i * N + j] - C2[i * N + j]) < 1e-10);
        }
      }
    }  /// END TEST
  }    /// END MAIN LOOP

  MPI_Finalize();
}
