#include "MMM.hpp"

#include <mpi.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <exception>
#include <fstream>
#include <iostream>
bool print = 0;

extern "C" {
void dgemm_(const char& TRANSA, const char& TRANSB, const int& M, const int& N,
            const int& K, const double& alpha, const double* A, const int& LDA,
            const double* B, const int& LDB, const double& beta, double* C,
            const int& LDC);
}

void transpose(matrix_t& A, const std::size_t N) {
  matrix_t temp(N * N);
  for (std::size_t i = 0; i < N; ++i) {
    for (std::size_t j = 0; j < N; ++j) {
      temp[i * N + j] = A[j * N + i];
    }
  }
  A = temp;
}

void naive_MMM(const matrix_t& A, const matrix_t& B, matrix_t& C,
               const std::size_t N) {
  for (std::size_t i = 0; i < N; ++i) {
    for (std::size_t k = 0; k < N; ++k) {
      for (std::size_t j = 0; j < N; ++j) {  // ikj is very $
        C[i * N + j] += A[i * N + k] * B[k * N + j];
      }
    }
  }
}

void distributeMatrixTo2Dgrid(const matrix_t& A, matrix_t& Aij,
                              const std::size_t grid_size) {
  int rank, P;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &P);
  assert(P >= grid_size * grid_size);

  const std::size_t N = std::sqrt(A.size());
  const std::size_t n = N / grid_size;

  Aij.resize(n * n);

  if (rank == 0) {
    for (std::size_t i = 0; i < N; ++i) {
      if (i < n) {
        std::size_t j;
        for (j = 0; j < n; ++j) {
          Aij[i * n + j] = A[i * N + j];
        }
        for (; j < N; j += n) {
          MPI_Send(
              &A[i * N + j], n, MPI_DOUBLE,
              getProcessorRank({i / n, j / n, 0}, grid_size * grid_size, 1), 0,
              MPI_COMM_WORLD);
        }
      } else {
        for (std::size_t j = 0; j < N; j += n) {
          MPI_Send(
              &A[i * N + j], n, MPI_DOUBLE,
              getProcessorRank({i / n, j / n, 0}, grid_size * grid_size, 1), 0,
              MPI_COMM_WORLD);
        }
      }
    }
  } else {
    for (std::size_t i = 0; i < n; ++i) {
      MPI_Recv(&Aij[i * n], n, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
    }
  }
}

void reduce2DgridToMatrix(const matrix_t& Cij, matrix_t& C,
                          const std::size_t grid_size) {
  int rank, P;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &P);

  const std::size_t N = std::sqrt(C.size());
  const std::size_t n = N / grid_size;

  if (rank == 0) {
    for (std::size_t i = 0; i < N; ++i) {
      if (i < n) {
        std::size_t j;
        for (j = 0; j < n; ++j) {
          C[i * N + j] = Cij[i * n + j];
        }
        for (; j < N; j += n) {
          MPI_Recv(
              &C[i * N + j], n, MPI_DOUBLE,
              getProcessorRank({i / n, j / n, 0}, grid_size * grid_size, 1),
              MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
      } else {
        for (std::size_t j = 0; j < N; j += n) {
          MPI_Recv(
              &C[i * N + j], n, MPI_DOUBLE,
              getProcessorRank({i / n, j / n, 0}, grid_size * grid_size, 1),
              MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
      }
    }
  } else {
    for (std::size_t i = 0; i < n; ++i) {
      MPI_Send(&Cij[i * n], n, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD);
    }
  }
}

inline std::tuple<std::size_t, std::size_t, std::size_t> getProcessorIndices(
    const std::size_t rank, const std::size_t P, const std::size_t c) {
  const std::size_t sqpc = std::sqrt(P / c);
  return {rank % sqpc, (rank / sqpc) % sqpc, rank / (P / c)};
}

inline int getProcessorRank(
    const std::tuple<std::size_t, std::size_t, std::size_t> indices,
    const std::size_t P, const std::size_t c) {
  return std::get<0>(indices) * std::sqrt(P / c) + std::get<1>(indices) +
         std::get<2>(indices) * (P / c);
}

void MMM25d(matrix_t& Aij, matrix_t& Bij, matrix_t& Cij, const std::size_t N,
            const std::size_t c) {
  int rank, P;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &P);
  const std::size_t sqpc = std::sqrt(P / c);
  auto indices = getProcessorIndices(rank, P, c);
  const std::size_t i = std::get<0>(indices);
  const std::size_t j = std::get<1>(indices);
  const std::size_t k = std::get<2>(indices);

  assert(Aij.size() == N * N / P && Bij.size() == N * N / P &&
         Cij.size() == N * N / P);

  if (k == 0) {
    for (std::size_t l = 1; l < c; ++l) {
      const std::size_t rank_to_send_to = getProcessorRank({i, j, l}, P, c);
      MPI_Send(Aij.data(), Aij.size(), MPI_DOUBLE, rank_to_send_to, l,
               MPI_COMM_WORLD);
      MPI_Send(Bij.data(), Bij.size(), MPI_DOUBLE, rank_to_send_to, l + 1,
               MPI_COMM_WORLD);
    }
  } else {
    for (std::size_t l = 1; l < c; ++l) {
      const std::size_t rank_to_recv_from = getProcessorRank({i, j, 0}, P, c);
      MPI_Recv(Aij.data(), Aij.size(), MPI_DOUBLE, rank_to_recv_from, l,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      MPI_Recv(Bij.data(), Bij.size(), MPI_DOUBLE, rank_to_recv_from, l + 1,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
  }

  std::size_t s =
      (j - i + k * static_cast<std::size_t>(std::sqrt(P / (c * c * c)))) % sqpc;
  matrix_t A_local(Aij.size());
  MPI_Send(Aij.data(), Aij.size(), MPI_DOUBLE,
           getProcessorRank({i, s, k}, P, c), 0, MPI_COMM_WORLD);
  MPI_Recv(A_local.data(), A_local.size(), MPI_DOUBLE, MPI_ANY_SOURCE, 0,
           MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  if (rank == 2) {
    std::cout << "s: " << getProcessorRank({i, s, k}, P, c) << std::endl
              << std::endl;
    std::cout << "A_local: \n";
    for (std::size_t i = 0; i < std::sqrt(A_local.size()); ++i) {
      for (std::size_t j = 0; j < std::sqrt(A_local.size()); ++j) {
        std::cout << A_local[i * std::sqrt(Cij.size()) + j] << " ";
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }

  std::size_t s_prime =
      (i - j + k * static_cast<std::size_t>(std::sqrt(P / (c * c * c)))) % sqpc;
  matrix_t B_local(Bij.size());
  MPI_Send(Bij.data(), Bij.size(), MPI_DOUBLE,
           getProcessorRank({s_prime, j, k}, P, c), 1, MPI_COMM_WORLD);
  MPI_Recv(B_local.data(), B_local.size(), MPI_DOUBLE, MPI_ANY_SOURCE, 1,
           MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  if (rank == 2) {
    std::cout << "s': " << getProcessorRank({s_prime, j, k}, P, c) << std::endl
              << std::endl;
    std::cout << "B_local: \n";
    for (std::size_t i = 0; i < std::sqrt(A_local.size()); ++i) {
      for (std::size_t j = 0; j < std::sqrt(A_local.size()); ++j) {
        std::cout << A_local[i * std::sqrt(Cij.size()) + j] << " ";
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }

  matrix_t Cijk(Cij.size(), 0.);
  const std::size_t size = std::sqrt(A_local.size());
  naive_MMM(A_local, B_local, Cijk, size);
  //  dgemm_('T', 'T', size, size, size, 1., A_local.data(), size,
  //  B_local.data(), size, 0., Cijk.data(), size); transpose(Cijk, size);

  s = (j + 1) % sqpc;
  s_prime = (i + 1) % sqpc;

  for (std::size_t t = 1; t < std::sqrt(P / (c * c * c)); ++t) {
    MPI_Send(Aij.data(), Aij.size(), MPI_DOUBLE,
             getProcessorRank({i, s, k}, P, c), 0, MPI_COMM_WORLD);
    MPI_Recv(A_local.data(), A_local.size(), MPI_DOUBLE, MPI_ANY_SOURCE, 0,
             MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Send(Bij.data(), Bij.size(), MPI_DOUBLE,
             getProcessorRank({s_prime, j, k}, P, c), 1, MPI_COMM_WORLD);
    MPI_Recv(B_local.data(), B_local.size(), MPI_DOUBLE, MPI_ANY_SOURCE, 1,
             MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    //    transpose(Cijk, size);
    //    dgemm_('T', 'T', size, size, size, 1., A_local.data(), size,
    //    B_local.data(), size, 1., Cijk.data(), size); transpose(Cijk, size);
    naive_MMM(A_local, B_local, Cijk, size);
  }

  if (rank == 2) {
    std::cout << "Cijk: \n";
    for (std::size_t i = 0; i < std::sqrt(Cijk.size()); ++i) {
      for (std::size_t j = 0; j < std::sqrt(Cijk.size()); ++j) {
        std::cout << Cijk[i * std::sqrt(Cijk.size()) + j] << " ";
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }

  //  MPI_Reduce(Cijk.data(), Cij.data(), Cijk.size(), MPI_DOUBLE, MPI_SUM, rank
  //  % (P / c), MPI_COMM_WORLD);
  Cij = Cijk;
}

void MMM(const matrix_t& A, const matrix_t& B, matrix_t& C, const size_t M) {
  // DEBUG
  //
  // std::ofstream fout("debug.txt");
  //
  // DEBUG END

  std::size_t bigRows, bigCols, i, j, k;
  constexpr std::size_t smallBlock = 8;

  int rank, P;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &P);

  const std::size_t sqrt_P = static_cast<size_t>(std::sqrt(P));
  const std::size_t sqpb = sqrt_P * smallBlock;

  matrix_t C_local(M * M, 0.);
  // matrix_t A_block(smallBlock * sqpb);
  // matrix_t B_block(smallBlock * sqpb);
  for (bigRows = 0; bigRows < M; bigRows += sqpb) {
    for (bigCols = 0; bigCols < M; bigCols += sqpb) {
      for (i = bigRows + (rank / sqrt_P) * smallBlock;
           i < bigRows + (rank / sqrt_P + 1) * smallBlock; ++i) {
        for (k = 0; k < M; ++k) {
          for (j = bigCols + (rank % sqrt_P) * smallBlock;
               j < bigCols + (rank % sqrt_P + 1) * smallBlock; ++j) {
            C_local[i * M + j] += A[i * M + k] * B[k * M + j];
          }
        }
      }

      // Start broadcasting
      if (rank == 0) {
        for (i = 0; i < smallBlock; ++i) {
          auto beg = C_local.data() +
                     (i + bigRows + (rank / sqrt_P) * smallBlock) * M +
                     bigCols + (rank % sqrt_P) * smallBlock;
          auto target = C.data() +
                        (i + bigRows + (rank / sqrt_P) * smallBlock) * M +
                        bigCols + (rank % sqrt_P) * smallBlock;
          std::copy(beg, beg + smallBlock, target);
        }
        for (int source = 1; source < P; ++source) {
          for (i = 0; i < smallBlock; ++i) {
            auto success =
                MPI_Recv(&C[(bigRows + i + (source / sqrt_P) * smallBlock) * M +
                            bigCols + (source % sqrt_P) * smallBlock],
                         smallBlock, MPI_DOUBLE, source, MPI_ANY_TAG,
                         MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            if (success != MPI_SUCCESS)
              throw std::runtime_error("RECEIVE FAILED");
          }
        }
      } else {
        for (i = bigRows + (rank / sqrt_P) * smallBlock;
             i < bigRows + (rank / sqrt_P + 1) * smallBlock; ++i) {
          /*          for(int k = bigCols + (rank % sqrt_P) * smallBlock; k <
             bigCols + (rank % sqrt_P + 1) * smallBlock; ++k){ fout <<
             C_local[i*M + k] << " ";
                    }
                    fout << std::endl;
          */
          MPI_Send(&C_local[i * M + bigCols + (rank % sqrt_P) * smallBlock],
                   smallBlock, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
        }
      }
    }
  }

  //  fout.close();
}
