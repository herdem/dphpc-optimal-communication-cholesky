#pragma once

#include <mpi.h>

#include <cmath>
#include <vector>

using index_t = std::size_t;
using matrix_t = std::vector<double>;

int lcm(int, int);

// basic prime Factorization
std::vector<index_t> primeFactorization(index_t);

// helper function to calculate a nice v
index_t vcalculation(index_t, index_t, index_t, index_t, index_t);

//!
//! Calculate the best processor grid decomposition (2.5D grid).
//! \param P number of processors
//! \return tuple containing the size of the first/second dimension and the size
//! of the third dimension
//!
std::tuple<index_t, index_t> calculateDecomposition(const int&);

//!
//! Calculates the parameters needed for the Cholesky function. Gives a fitting
//! matrix size.
//! \param inpN dimension of the matrix at input
//! \param inpP number of processors at input
//! \param mult multiplier for tile size
//! \return tuple containing the size of the first/second dimension of the
//! processor grid, size of the third dimension of the processor grid,
//! tile size and the actual, fitted dimension of the matrix to be used
//!
std::tuple<index_t, index_t, index_t, index_t> calculateParameters(
    index_t inpN, index_t inpP);
