#include "func_scal.hpp"

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

//! basic Prime Factorization could be runtime improved
std::vector<index_t> primeFactorization(index_t N){
	index_t i = 2;
	std::vector<index_t> primes;
	//divide trough all possible numbers up to N
	while(i <= N){

		//divide an write down the found dividor
		if(N % i == 0){
			N = N/i;
			primes.push_back(i);

		//increment, since all primes are odd we can skip some numbers
		//could be improved through sieve of Eratosthenes (or just a library call)
		}else if(i == 2){
			i++;
		}else{
			i += 2;
		}
	}
	return primes;
}

//try to calculate a v (otherwise attempt again with better N)
index_t vcalculation(index_t N, index_t sqrtp1, index_t c, index_t lower_bound, index_t upper_bound){
	
	//let's start of with a prime factorization
	std::vector<index_t> primes = primeFactorization(N);
	
	//baseline v
	index_t v = c;

	//Gray code: this binary encryption attempts all different combinations.
	index_t len = primes.size();
	std::vector<index_t> gray(len,0);
	
	index_t max_k = len*len -1;
	index_t v_try, numb, i;
	//loop over all possible numbers, ~2^primes 
	for(index_t k = 0; k < max_k; ++k){
	  v_try = v;
	  numb = k;
	  std::fill(gray.begin(),gray.end(),0);
	  i = 0;

	  //encrypt our number into binaries
	  while(numb > 0){
		  gray[i] = numb%2;
		  numb = numb/2;
		  i++;
		  }
	  //decrypt it into a possible v 	  
	  for(index_t p = 0; p < len; ++p){
	  	if(gray[p] == 1){
			v_try *= primes[p];
		}
	  }
	  //is our v useful?
	  if(v_try < upper_bound && v_try > lower_bound){
	  	v = v_try;
		// we only need one good v
		return v;
	  }	
	}
	return v;
}

//! Calculate the best processor grid decomposition (2.5D grid).
//! \param P number of processors
//! \return tuple containing the size of the first/second dimension and the size
//! of the third dimension
//!
std::tuple<index_t, index_t> calculateDecomposition(const int& P) {
  index_t p13 = std::floor(std::pow(P + 1, 1. / 3.));
  index_t ppp = std::floor(std::sqrt(P));
  index_t c = 1;
  index_t best_ppp = ppp;
  index_t best_c = c;
  index_t P1;
  double bestCost = 1. / (ppp * c);
  //!------------------- Init -------------------
  //! p13 = upper limit of c; The number of different 2-D grids will not be more
  //! than the third root of total processes. P1 = #processes on one 2-D grid. c
  //! = size of the third dimension starting with 1 and increasing linearly
  //! until maximum found. ppp = length to calculate process grid sizes in one
  //! 2D plane. The space is at most the square root of #processes on one 2-D
  //! grid. Starting with P1 = P and decreasing once a better decomposition is
  //! found. bestCost = our best decomposition that we have found yet.
  //! Calculated with 1/(ppp*c). best_ppp & best_c are running variables of
  //! possible decompositions used to find best values of ppp and c
  //! respectively.
  //!--------------------------------------------

  //! Find a better cost by slowly increasing the number of 2D grids c.
  while (c <= p13) {
    //! must update based on one more 2D grid
    P1 = std::floor(1. * P / c);
    ppp = std::floor(std::sqrt(P1));
    double cost = 1. / (ppp * c);
    if (cost < bestCost) {
      //! we have found a more cost efficient decomposition!
      //! set this to the new bestcost and try with a different c.
      bestCost = cost;
      best_ppp = ppp;
      best_c = c;
    }
    ++c;
  }
  //! if thrown than we have assigned more processes than we actually have in
  //! our communicator.
  if (best_ppp * best_ppp * best_c > P)
    throw std::runtime_error("no p and c decomposition found!");
  return std::make_tuple(best_ppp, best_c);
}

//! Calculates the parameters needed for the Cholesky function. Gives a fitting
//! matrix size.
//! \param inpN dimension of the matrix at input
//! \param inpP number of processors at input
//! \param mult multiplier for tile size
//! \return tuple containing the size of the first/second dimension of the
//! processor grid, size of the third dimension of the processor grid,
//! tile size and the actual, fitted dimension of the matrix to be used
//!
std::tuple<index_t, index_t, index_t, index_t> calculateParameters(
    index_t inpN, index_t inpP) {
  std::tuple<index_t, index_t> p1_c = calculateDecomposition(inpP);
  index_t sqrtp1 = std::get<0>(p1_c);
  index_t c = std::get<1>(p1_c);
  //start of v calculation
  index_t factor = sqrtp1 * c;
  index_t small_N = std::ceil(1. * inpN/factor);
  //We want v to be inside a bound
  index_t lower_bound = std::min(std::floor(std::sqrt(factor * small_N)),std::floor(256/1.1));
  index_t upper_bound = std::min(static_cast<int>(1.5*lower_bound),256);
  index_t v = upper_bound + 1;
  //Searches for a v that minimizes size difference from N and input N
  //under the condition that v is inside the bounds.
  while(v > upper_bound || v < lower_bound){
   	v = vcalculation(small_N, sqrtp1, c, lower_bound,upper_bound);
    //incrementing only by one results in terrible runtime for N > 10^5
    if(v > upper_bound || v < lower_bound) small_N += 1;
  }
  index_t N = small_N * factor;
  //end of v calculation	
  return std::make_tuple(sqrtp1, c, v, N);
}

