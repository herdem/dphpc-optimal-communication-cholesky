#include "func_scal.hpp"
#include <mpi.h>
#include <mkl_scalapack.h>
#include <cmath>
#include <cassert>
#include <vector>
#include <iostream>
#include <iomanip>
#include <liblsb.h>
#include <tuple>

// based on: https://gist.github.com/leopoldcambier/be8e68906ecfd7f03edf0d809db37cc1

using index_t = std::size_t;

extern "C" {
void blacs_pinfo_(int*, int*);
void blacs_get_(int*, int*, int*);
void blacs_gridinit_(int*, char*, int*, int*);
void blacs_gridinfo_(int*, int*, int*, int*, int*);
void blacs_gridexit_(int*);
int blacs_pnum_(int*, int*, int*);
}

int main(int argc, char *argv[]) {
  MPI_Init(nullptr, nullptr);
  
  LSB_Init("scalapack_pdpotrf", 0);
  
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
//  if (rank == 0) {
//    const PAPI_hw_info_t *hwinfo = nullptr;
//    if ((hwinfo = PAPI_get_hardware_info()) == nullptr) {
//      std::cerr << "Couldn't retrieve hardware info.\n";
//      return -1;
//    }
//    auto space = std::setw(15);
//    std::cout << space << "#CPUs:" << space << hwinfo->ncpu << std::endl;
//    std::cout << space << "#nodes:" << space << hwinfo->nnodes << std::endl;
//    std::cout << space << "#totalCPUs:" << space << hwinfo->totalcpus << std::endl;
//    std::cout << space << "vendor:" << space << hwinfo->vendor << std::endl;
//    std::cout << space << "vendor_string:" << space << hwinfo->vendor_string << std::endl;
//    std::cout << space << "model:" << space << hwinfo->model << std::endl;
//    std::cout << space << "model_string:" << space << hwinfo->model_string << std::endl;
//    std::cout << space << "revision:" << space << hwinfo->revision << std::endl;
//    std::cout << space << "mhz:" << space << hwinfo->mhz << std::endl;
//  }
  
  constexpr unsigned int runs = 1;
  
  if (argc != 6) {
    return -1;
  }
  int blacs_rows = std::atoi(argv[1]);
  int blacs_cols = std::atoi(argv[2]);
  const int lower_bound_N = std::atoi(argv[3]);
  const int upper_bound_N = std::atoi(argv[4]);
  const int step_size = std::atoi(argv[5]);
  assert(size == blacs_rows * blacs_cols);
  
  int blacs_rank, blacs_size;
  blacs_pinfo_(&blacs_rank, &blacs_size);
  int zero = 0, blacs_context;
  blacs_get_(&zero, &zero, &blacs_context);
  // use column-major grid ordering as in our program
  char blacs_layout = 'C';
  blacs_gridinit_(&blacs_context, &blacs_layout, &blacs_rows, &blacs_cols);
  
  LSB_Set_Rparam_int("P", size);
  LSB_Set_Rparam_int("p_rows", blacs_rows);
  LSB_Set_Rparam_int("p_cols", blacs_cols);
  
  int p_i, p_j;
  blacs_gridinfo_(&blacs_context, &blacs_rows, &blacs_cols, &p_i, &p_j);
  
  for (int inpN = lower_bound_N; inpN <= upper_bound_N; inpN += step_size) {
    LSB_Set_Rparam_int("inpN", inpN);
    
    int v_rows = blacs_rows, v_cols = blacs_cols;
      
      //calculate v, N
      auto params = calculateParameters(inpN, size);
      v_rows = std::get<2>(params);
      v_cols = v_rows; 
      int N = std::get<3>(params);

      LSB_Set_Rparam_int("N", N); 
      LSB_Set_Rparam_int("tile_size_rows", v_rows);
      LSB_Set_Rparam_int("tile_size_cols", v_cols);
      
      int local_rows = numroc_(&N, &v_rows, &p_i, &zero, &blacs_rows);
      int local_cols = numroc_(&N, &v_cols, &p_j, &zero, &blacs_cols);
      
      std::vector<double> A_local(local_rows * local_cols);
      unsigned int glob_i, glob_j;
      for (unsigned int j = 0; j < local_cols; ++j) {
        glob_j = p_j * v_cols + (j / v_cols) * blacs_cols * v_cols + j;
        for (unsigned int i = 0; i < local_rows; ++i) {
          glob_i = p_i * v_rows + (i / v_rows) * blacs_rows * v_rows + i;
          if (glob_i == glob_j) {
            A_local[i + j * local_rows] = N * N;
          } else {
            A_local[i + j * local_rows] = glob_j + glob_i;
          }
        }
      }
      
      int descriptor[9] = {1, blacs_context, N, N, v_rows, v_cols, 0, 0, local_rows};
      
      std::vector<double> A_local_copy = A_local;
      
      char uplo = 'L';
      int one = 1, info;
      // do actual computation
      for (unsigned int i = 0; i < runs; ++i) {
        A_local = A_local_copy;
        MPI_Barrier(MPI_COMM_WORLD);
        LSB_Res();
        pdpotrf(&uplo, &N, A_local.data(), &one, &one, descriptor, &info);
        LSB_Rec(i);
        if (info != 0) {
          std::cerr << "Error in pdpotrf with code: " << info << std::endl;
        }
      }
  }
  
  LSB_Finalize();
  
  // destroy blacs grid, finalize MPI
  blacs_gridexit_(&blacs_context);
  MPI_Finalize();
}
