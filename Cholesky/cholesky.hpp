#pragma once

#include <mpi.h>

#include <cmath>
#include <vector>

using index_t = std::size_t;
using matrix_t = std::vector<double>;

int lcm(int, int);

// basic prime Factorization
std::vector<index_t> primeFactorization(index_t);

// helper function to calculate a nice v
index_t vcalculation(index_t, index_t, index_t, index_t, index_t);

//!
//! Calculate the best processor grid decomposition (2.5D grid).
//! \param P number of processors
//! \return tuple containing the size of the first/second dimension and the size
//! of the third dimension
//!
std::tuple<index_t, index_t> calculateDecomposition(const int&);

//!
//! Calculates the parameters needed for the Cholesky function. Gives a fitting
//! matrix size.
//! \param inpN dimension of the matrix at input
//! \param inpP number of processors at input
//! \param mult multiplier for tile size
//! \return tuple containing the size of the first/second dimension of the
//! processor grid, size of the third dimension of the processor grid,
//! tile size and the actual, fitted dimension of the matrix to be used
//!
std::tuple<index_t, index_t, index_t, index_t> calculateParameters(
    index_t inpN, index_t inpP);

//!
//! Calculates the grid coordinates for a 2.5D column major processor grid.
//! \param p rank of the processor whose coordinates you want to know
//! \param p1 size of the front plane/the first two dimensions of the grid
//! \return tuple containing i, j and k coordinate
//!
std::tuple<index_t, index_t, index_t> p2X(int, const index_t);

//!
//! Calculates the rank for given coordinates in a 2.5D grid.
//! \param pi i-coordinate of the processor
//! \param pj j-coordinate of the processor
//! \param pk k-coordinate of the processor
//! \param p1 size of the front plane/the first two dimensions of the grid
//! \return rank in world communicator of the grid
//!
inline index_t X2p(const index_t pi, const index_t pj, const index_t pk,
                   const index_t p1) {
  return (pi + std::sqrt(p1) * pj + p1 * pk);
}

//!
//! Local-to-global tile mapping.
//! \param offset
//! \param ind local index
//! \param dim dimension to multiply with
//! \return global tile index
//!
inline index_t l2g(const index_t offset, const index_t ind, const index_t dim) {
  return ind * dim + offset;
}

//!
//! Computes the lower triangular Cholesky decomposition of a block-cyclicly
//! distributed matrix. It assumes a valid 2.5D processor grid, column-major
//! processor ordering.
//! \param L matrix (distributed in a block-cyclic manner on
//! the front processor plane) to be factorised; will contain the factorised
//! matrix in the end in the front processor plane of the grid
//! \param p1 size of one processor plane (2D)
//! \param c length of the third dimension of the processor grid
//! \param cols leading dimension of L, i.e. the number of cols
//! of the local matrix
//! \param N global dimension of matrix
//! \param v tiling size
//! \param world_comm communicator in which the function should operate
//!
void cholesky(matrix_t&, index_t, index_t, index_t, index_t, index_t, MPI_Comm);

//!
//! Distributes a matrix in block-cyclic manner.
//! \param A global matrix
//! \param A_buffer local reordered matrices - same size as a A
//! \param N dimension of the global matrix
//! \param v tile size
//! \param sqrtp1 size of the first/second dimension of the processor grid
//! \param p1 sqrtp1 * sqrtp1
//!
void distribute(const matrix_t&, matrix_t&, index_t, index_t, index_t, index_t);

//!
//! Assembles a matrix in block-cyclic manner. Reverse of distribute.
//! \param A global matrix
//! \param A_buffer local reordered matrices - same size as a A
//! \param N dimension of the global matrix
//! \param v tile size
//! \param sqrtp1 size of the first/second dimension of the processor grid
//! \param p1 sqrtp1 * sqrtp1
//!
void assemble(matrix_t&, const matrix_t&, index_t, index_t, index_t, index_t);

//!
//! Performs several timed runs of the cholesky decomposition for given matrix
//! that is potentially upscaled to fit the tile size. To be used to check for
//! correctness.
//! \param inpA input matrix (global) - rank 0 must have the data
//! \param params parameters for decomposition as calculated by
//! calculateParameters
//! \param world_comm communicator in which the function
//! should run
//! \param runs number of runs to perform to get statistical data
//!
void distributed_cholesky(matrix_t&,
                          std::tuple<index_t, index_t, index_t, index_t>,
                          MPI_Comm, index_t);
