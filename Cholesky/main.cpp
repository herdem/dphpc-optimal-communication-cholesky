#include <liblsb.h>
#include <mkl.h>
#include <mpi.h>

#include <cmath>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <vector>

#include "cholesky.hpp"

using index_t = std::size_t;
using matrix_t = std::vector<double>;

int main(int argc, char *argv[]) {
  MPI_Init(nullptr, nullptr);

  LSB_Init("Cholesky", 0);
  LSB_Rec_disable();

  constexpr unsigned int runs = 10;

  if (argc != 4) {
    return -1;
  }
  const index_t lower_N = std::atoi(argv[1]);
  const index_t upper_N = std::atoi(argv[2]);
  const index_t step_size = std::atoi(argv[3]);

  int rank, P;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &P);
  //  if (rank == 0) {
  //    const PAPI_hw_info_t* hwinfo = nullptr;
  //    if ((hwinfo = PAPI_get_hardware_info()) == nullptr) {
  //      std::cerr << "Couldn't retrieve hardware info.\n";
  //      return -1;
  //    }
  //    auto space = std::setw(15);
  //    std::cout << space << "#CPUs:" << space << hwinfo->ncpu << std::endl;
  //    std::cout << space << "#nodes:" << space << hwinfo->nnodes << std::endl;
  //    std::cout << space << "#totalCPUs:" << space << hwinfo->totalcpus
  //    << std::endl;
  //    std::cout << space << "vendor:" << space << hwinfo->vendor << std::endl;
  //    std::cout << space << "vendor_string:" << space << hwinfo->vendor_string
  //    << std::endl;
  //    std::cout << space << "model:" << space << hwinfo->model << std::endl;
  //    std::cout << space << "model_string:" << space << hwinfo->model_string
  //    << std::endl;
  //    std::cout << space << "revision:" << space << hwinfo->revision <<
  //    std::endl; std::cout << space << "mhz:" << space << hwinfo->mhz <<
  //    std::endl;
  //  }

  const auto proc_grid = calculateDecomposition(P);
  const int sqrtp1 = std::get<0>(proc_grid);
  const int c = std::get<1>(proc_grid);
  const int p1 = sqrtp1 * sqrtp1;
  const int newP = p1 * c;

  LSB_Set_Rparam_int("inpP", P);
  LSB_Set_Rparam_int("p1", sqrtp1 * sqrtp1);
  LSB_Set_Rparam_int("c", c);
  LSB_Set_Rparam_int("actual_P", newP);

  // create world_comm
  MPI_Comm world_comm;
  MPI_Comm_split(MPI_COMM_WORLD, rank / newP, rank, &world_comm);

  if (rank < newP) {
    for (std::size_t N = lower_N; N <= upper_N; N += step_size) {
      LSB_Set_Rparam_int("inpN", N);

      // parameter tuning
      auto params = calculateParameters(N, P);
      const index_t v = std::get<2>(params);
      const index_t newN = std::get<3>(params);
      const index_t lcm_v = lcm(sqrtp1, c);
      
      LSB_Set_Rparam_int("tile_size", v);
      LSB_Set_Rparam_int("actual_N", newN);

      const index_t local_size = newN / sqrtp1;

      // create local matrix
      matrix_t A_local(local_size * local_size, 0.),
          A_local_copy(local_size * local_size, 0.);
      if (rank < p1) {
        const auto coords = p2X(rank, p1);
        const index_t p_i = std::get<0>(coords);
        const index_t p_j = std::get<1>(coords);
        index_t glob_i, glob_j;
        for (index_t i = 0; i < local_size; ++i) {
          glob_i = p_i * v + (i / v) * sqrtp1 * v + i;
          for (index_t j = 0; j < local_size; ++j) {
            glob_j = p_j * v + (j / v) * sqrtp1 * v + j;
            if (glob_i == glob_j) {
              A_local[i * local_size + j] = newN * newN;
            } else {
              A_local[i * local_size + j] = glob_i + glob_j;
            }
          }
        }
        A_local_copy = A_local;
      }

      LSB_Rec_enable();
      for (index_t run = 0; run < runs; ++run) {
        A_local = A_local_copy;
        // actual computation and measure
        MPI_Barrier(world_comm);
        LSB_Res();
        cholesky(A_local, p1, c, local_size, newN, v, world_comm);
        LSB_Rec(run);
      }
      LSB_Rec_disable();
      
    }
  }

  MPI_Comm_free(&world_comm);

  LSB_Finalize();
  MPI_Finalize();
}
