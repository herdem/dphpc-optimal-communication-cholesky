#include "cholesky.hpp"

#include <liblsb.h>
#include <mkl.h>
#include <mpi.h>

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

inline int gcd(int a, int b) {
  int h;
  if (a == 0) return std::abs(b);
  if (b == 0) return std::abs(a);
  
  do {
    h = a % b;
    a = b;
    b = h;
  } while (b != 0);
  
  return std::abs(a);
}

int lcm(int a, int b) { return std::abs(a * b) / gcd(a, b); }


//! basic Prime Factorization could be runtime improved
std::vector<index_t> primeFactorization(index_t N){
	index_t i = 2;
	std::vector<index_t> primes;
	//divide trough all possible numbers up to N
	while(i <= N){

		//divide an write down the found dividor
		if(N % i == 0){
			N = N/i;
			primes.push_back(i);

		//increment, since all primes are odd we can skip some numbers
		//could be improved through sieve of Eratosthenes (or just a library call)
		}else if(i == 2){
			i++;
		}else{
			i += 2;
		}
	}
	return primes;
}

//try to calculate a v (otherwise attempt again with better N)
index_t vcalculation(index_t N, index_t sqrtp1, index_t c, index_t lower_bound, index_t upper_bound){
	
	//let's start of with a prime factorization
	std::vector<index_t> primes = primeFactorization(N);
	
	//baseline v
	index_t v = c;

	//Gray code: this binary encryption attempts all different combinations.
	index_t len = primes.size();
	std::vector<index_t> gray(len,0);
	
	index_t max_k = len*len -1;
	index_t v_try, numb, i;
	//loop over all possible numbers, ~2^primes 
	for(index_t k = 0; k < max_k; ++k){
	  v_try = v;
	  numb = k;
	  std::fill(gray.begin(),gray.end(),0);
	  i = 0;

	  //encrypt our number into binaries
	  while(numb > 0){
		  gray[i] = numb%2;
		  numb = numb/2;
		  i++;
		  }
	  //decrypt it into a possible v 	  
	  for(index_t p = 0; p < len; ++p){
	  	if(gray[p] == 1){
			v_try *= primes[p];
		}
	  }
	  //is our v useful?
	  if(v_try < upper_bound && v_try > lower_bound){
	  	v = v_try;
		// we only need one good v
		return v;
	  }
	}
	return v;
}

//!
//! Calculate the best processor grid decomposition (2.5D grid).
//! \param P number of processors
//! \return tuple containing the size of the first/second dimension and the size
//! of the third dimension
//!
std::tuple<index_t, index_t> calculateDecomposition(const int& P) {
index_t c = std::floor(std::pow(P,1./3.));
index_t ppp = c;
index_t best_c = c;
index_t relax = c/4 + 1;
c -=relax;
if (c < 1){
  c = 1;
 }
index_t best_ppp = ppp;
index_t P1;
while (c <= relax + std::floor(std::pow(P,1./3.)) && c <= ppp) {
  P1 = std::floor(1. * P / c);
  ppp = std::floor(std::sqrt(P1));
  //try to use more processes                                                                                                                                                                                                                                                   
  if (P-ppp*ppp*c < P-best_ppp*best_ppp*best_c) {
    //save                                                                                                                                                                                                                                                                      
    best_ppp = ppp;
    best_c = c;
  }
  ++c;
 }                                                                                           
if (best_ppp * best_ppp * best_c > P)
  throw std::runtime_error("no p and c decomposition found!");
return std::make_tuple(best_ppp, best_c);
}

//!
//! Calculates the parameters needed for the Cholesky function. Gives a fitting
//! matrix size.
//! \param inpN dimension of the matrix at input
//! \param inpP number of processors at input
//! \return tuple containing the size of the first/second dimension of the
//! processor grid, size of the third dimension of the processor grid,
//! tile size and the actual, fitted dimension of the matrix to be used
//!
std::tuple<index_t, index_t, index_t, index_t> calculateParameters(
    index_t inpN, index_t inpP) {
  std::tuple<index_t, index_t> p1_c = calculateDecomposition(inpP);
  index_t sqrtp1 = std::get<0>(p1_c);
  index_t c = std::get<1>(p1_c);
  //start of v calculation
  index_t factor = sqrtp1 * c;
  index_t small_N = std::ceil(1. * inpN/factor);
  //We want v to be inside a bound
  index_t lower_bound = std::min(std::floor(std::sqrt(factor * small_N)),std::floor(256/1.1));
  index_t upper_bound = std::min(static_cast<int>(1.8*lower_bound),256);
  index_t v = upper_bound + 1;
  //Searches for a v that minimizes size difference from N and input N
  //under the condition that v is inside the bounds.
  while(v > upper_bound || v < lower_bound){
   	v = vcalculation(small_N, sqrtp1, c, lower_bound,upper_bound);
    //incrementing only by one results in terrible runtime for N > 10^5
    if(v > upper_bound || v < lower_bound) small_N += 1;
  }
  index_t N = small_N * factor;
  //end of v calculation	
  return std::make_tuple(sqrtp1, c, v, N);
}

//!
//! Calculates the grid coordinates for a 2.5D column major processor grid.
//! \param p rank of the processor whose coordinates you want to know
//! \param p1 size of the front plane/the first two dimensions of the grid
//! \return tuple containing i, j and k coordinate
//!
std::tuple<index_t, index_t, index_t> p2X(int p, const index_t p1) {
  index_t pk = std::floor(p / p1);
  p -= pk * p1;
  index_t pj = std::floor(p / std::sqrt(p1));
  index_t pi = p % static_cast<int>(std::sqrt(p1));
  return std::make_tuple(pi, pj, pk);
}

//!
//! Computes the lower triangular Cholesky decomposition of a block-cyclicly
//! distributed matrix. It assumes a valid 2.5D processor grid, column-major
//! processor ordering.
//! \param L matrix (distributed in a block-cyclic manner on
//! the front processor plane) to be factorised; will contain the factorised
//! matrix in the end in the front processor plane of the grid
//! \param p1 size of one processor plane (2D)
//! \param c length of the third dimension of the processor grid
//! \param cols leading dimension of L, i.e. the number of cols
//! of the local matrix
//! \param N global dimension of matrix
//! \param v tiling size
//! \param world_comm communicator in which the function should operate
//!
void cholesky(matrix_t& L, index_t p1, index_t c, index_t cols, index_t N,
              index_t v, MPI_Comm world_comm) {
#ifdef TIME
  auto start = std::chrono::high_resolution_clock::now();
#endif

  //!----------------------------------------------------------
  //! Setup of MPI and other needed variables
  //!----------------------------------------------------------
  MPI_Pcontrol(1, "cholesky");
  int world_rank, world_size;
  MPI_Comm_rank(world_comm, &world_rank);
  MPI_Comm_size(world_comm, &world_size);

  const index_t sqrtp1 = std::sqrt(
      p1);  //!< size of the dimension of the 2D grid, i.e. front plane
  const index_t Nt =
      std::ceil(static_cast<double>(N) /
                v);  //!< number of global tiles in one coordinate direction
  const index_t tA10 = std::ceil(static_cast<double>(Nt) /
                           world_size);  //!< number of tiles in the A10buffer
  const index_t tA11 = std::ceil(static_cast<double>(Nt) /
                           sqrtp1);  //!< number of tiles in the A11buffer in
                                     //!< one coordinate direction
  const index_t nlayr = v / c;    //!< size of the tiles for the 2.5D MMM

  MPI_Status status;
  status.MPI_ERROR = 0;
  int buff_size;
  MPI_Status statuses[tA10];
  for(int i = 0; i < tA10; ++i){
    statuses[i].MPI_ERROR = 0;
  }

  //! assert valid grid
  if (world_size != p1 * c)
    throw std::invalid_argument("Invalid processor grid.");

  //! assert valid sizes and therefore inputs
  assert(tA11 == cols / v);

#ifndef NDEBUG
  //! get some debug parameters
  if (world_rank == 0) {
    std::cerr << "N: " << N << std::endl;
    std::cerr << "p1: " << p1 << std::endl;
    std::cerr << "c: " << c << std::endl;
    std::cerr << "v: " << v << std::endl;
    std::cerr << "Nt: " << Nt << std::endl;
    std::cerr << "tA10: " << tA10 << std::endl;
    std::cerr << "tA11: " << tA11 << std::endl;
    std::cerr << "nlayr: " << nlayr << std::endl;
  }
#endif

  //! get the coordinate of your rank in the 2.5D grid
  auto p_coord = p2X(world_rank, p1);
  const index_t p_i = std::get<0>(p_coord), p_j = std::get<1>(p_coord),
                p_k = std::get<2>(p_coord);

  //! create useful communicators: the name defines which coordinate can be
  //! varied in this comm we assume i-j-k coordinate system
  MPI_Comm ij_comm, jk_comm, j_comm, k_comm;
  MPI_Comm_split(world_comm, p_i, world_rank, &jk_comm);
  MPI_Comm_split(world_comm, p_k, world_rank, &ij_comm);
  MPI_Comm_split(ij_comm, p_i, world_rank, &j_comm);
  MPI_Comm_split(jk_comm, p_j, world_rank, &k_comm);

  int jk_rank, jk_size;
  MPI_Comm_rank(jk_comm, &jk_rank);
  MPI_Comm_size(jk_comm, &jk_size);

  //! create datatype for A11 matrix tiles; contains one tile in the A11 buffer
  //! with extent to the next tile below it
  MPI_Datatype A11_tile_temp, A11_tile_type;
  const int array_of_sizes[2] = {static_cast<int>(tA11 * v),
                                 static_cast<int>(tA11 * v)};
  const int array_of_subsizes[2] = {static_cast<int>(v), static_cast<int>(v)};
  const int array_of_starts[2] = {0, 0};
  MPI_Type_create_subarray(2, array_of_sizes, array_of_subsizes,
                           array_of_starts, MPI_ORDER_C, MPI_DOUBLE,
                           &A11_tile_temp);
  MPI_Type_create_resized(A11_tile_temp, 0, tA11 * v * v * sizeof(double),
                          &A11_tile_type);
  MPI_Type_commit(&A11_tile_type);

  //! create datatype for 2.5D distribution; contains one stripe of a tile with
  //! extent to the next stripe; to be used in the 2.5D sense
  MPI_Datatype A10_nlayr_temp, A10_nlayr_type;
  const int a_s_2[2] = {static_cast<int>(tA10 * v), static_cast<int>(v)};
  const int a_subs_2[2] = {static_cast<int>(v), static_cast<int>(nlayr)};
  MPI_Type_create_subarray(2, a_s_2, a_subs_2, array_of_starts, MPI_ORDER_C,
                           MPI_DOUBLE, &A10_nlayr_temp);
  MPI_Type_create_resized(A10_nlayr_temp, 0, nlayr * sizeof(double),
                          &A10_nlayr_type);
  MPI_Type_commit(&A10_nlayr_type);

  //! requests for nonblocking MPI communication
  MPI_Request reqA00;
  MPI_Request req_gat[tA10 * c];
  MPI_Request req_arr[tA10];
  index_t req_count = 0;
  std::vector<MPI_Request> req_vec;
  req_vec.reserve(tA11 * (tA11 - 1) / 2 + tA11);

  //! buffers to be used
  matrix_t A00buff(v * v, 0.);         //!< the current diagonal tile
  matrix_t A10buff(tA10 * v * v, 0.);  //!< the current column tiles
  matrix_t A10buffRcv(tA10 * jk_size * v * nlayr,
                      0.);  //!< the buffers for updating the matrix to the
                            //!< right of the current column
  matrix_t A01buffRcv(tA10 * jk_size * v * nlayr, 0.);
  matrix_t& A11buff = L;  //!< our actual matrix but renamed for consistency

  //! displacements for MPI_*v calls
  std::vector<int> displs_jk(jk_size), displs_k_vn(jk_size),
      displs_j_vn(jk_size);
  #pragma vector
  for (index_t i = 0; i < jk_size; ++i) {
    displs_jk[i] = i;
    displs_k_vn[i] = i * sqrtp1 * v * nlayr;
    displs_j_vn[i] = i * v * nlayr;
  }

  //!----------------------------------------------------------
  //! Initial data distribution
  //!----------------------------------------------------------

  //! everyone needs A00, so send it to everyone
  if (world_rank == 0) {
    #pragma vector
    for (index_t i = 0; i < v; ++i) {
      #pragma vector
      for (index_t j = 0; j < v; ++j) {
        A00buff[i * v + j] = L[i * cols + j];
      }
    }
  }
  MPI_Ibcast(A00buff.data(), v * v, MPI_DOUBLE, 0, world_comm, &reqA00);

  //! fill current column buffer with first column
  std::vector<int> sendcounts(jk_size, 0);
  for (index_t lti = 0; lti < tA10; ++lti) {
    //! fill sendcounts with 1 as long as there are tiles
    if (p_j == 0 && p_k == 0) {
      std::fill(sendcounts.begin(), sendcounts.end(), 0);
      for (index_t i = 0; i < jk_size; ++i) {
        if (lti * jk_size + i < tA11) {
          sendcounts[i] = 1;
        }
      }
    }
    //! scatter from the first and front processor column to the others in the
    //! jk-plane
    MPI_Iscatterv(&L[lti * v * jk_size * cols], sendcounts.data(),
                  displs_jk.data(), A11_tile_type, &A10buff[lti * v * v], v * v,
                  MPI_DOUBLE, 0, jk_comm, &req_arr[lti]);
  }

#ifdef TIME
  std::vector<std::chrono::duration<double>> times(
      10, std::chrono::high_resolution_clock::duration::zero());
  times[0] = std::chrono::high_resolution_clock::now() - start;
  start = std::chrono::high_resolution_clock::now();
#endif

  //!----------------------------------------------------------
  //! Main loop
  //!----------------------------------------------------------

  for (index_t k = 0; k < Nt; ++k) {
    MPI_Wait(&reqA00, &status); //!< wait for the diagonal tile to arrive
    if(status.MPI_ERROR != 0) throw std::runtime_error("Wait failed for rank " + std::to_string(world_rank) + " with Errorcode " + std::to_string(status.MPI_ERROR));

    //! do actual cholesky computation of the current diagonal tile
    int error = LAPACKE_dpotrf(LAPACK_ROW_MAJOR, 'L', v, A00buff.data(), v);
    if (error > 0) {
      throw std::domain_error(
          "Input matrix is not symmetric positive definite in iteration " +
          std::to_string(k) + ".");
    }

#ifdef TIME
    times[1] += std::chrono::high_resolution_clock::now() - start;
    start = std::chrono::high_resolution_clock::now();
#endif

    //! update the column below the current diagonal tile; solving a system of
    //! linear equations with multiple right hand sides newA10 * A00^T = oldA10
    for (index_t ltiA10 = std::max(static_cast<int>(k / world_size), static_cast<int>(std::ceil(k + 1. - world_rank) / world_size)); 
        ltiA10 < tA10 && l2g(world_rank, ltiA10, world_size) < Nt; ++ltiA10) {
        MPI_Wait(&req_arr[ltiA10], &status); //!< wait for the column tile to arrive
        if(status.MPI_ERROR != 0) throw std::runtime_error("Wait failed for rank " + std::to_string(world_rank) + " with Errorcode " + std::to_string(status.MPI_ERROR));	

        cblas_dtrsm(CblasRowMajor, CblasRight,
                    CblasLower, CblasTrans,
                    CblasNonUnit, v, v, 1., A00buff.data(), v,
                    &A10buff[ltiA10 * v * v], v);
    }

#ifdef TIME
    times[2] += std::chrono::high_resolution_clock::now() - start;
    start = std::chrono::high_resolution_clock::now();
#endif

    //!----------------------------------------------------------
    //! Prepare 2.5D matrix update
    //!----------------------------------------------------------

    std::vector<int> sendscat(c), recvscat(c), recvgather(sqrtp1);
    for (index_t lti = k / world_size; lti < tA10; ++lti) {
      std::fill(recvscat.begin(), recvscat.end(), 0);
      #pragma vector
      for (int k_root = std::max(
               0, static_cast<int>(std::ceil(
                      (k + 1. - lti * world_size - p_i - p_j * sqrtp1) / p1)));
           k_root < c && l2g(X2p(p_i, p_j, k_root, p1), lti, world_size) < Nt;
           ++k_root) {
        recvscat[k_root] = v * nlayr;
      }

      std::fill(sendscat.begin(), sendscat.end(), recvscat[p_k] / (v * nlayr));

      //! reshuffle data along k-line
      MPI_Alltoallv(&A10buff[lti * v * v], sendscat.data(), displs_jk.data(),
                    A10_nlayr_type,
                    &A10buffRcv[(lti * jk_size + p_j) * v * nlayr],
                    recvscat.data(), displs_k_vn.data(), MPI_DOUBLE, k_comm);

      for (index_t ltk = 0; ltk < c; ++ltk) {
        std::fill(recvgather.begin(), recvgather.end(), 0);
        #pragma vector
        for (index_t j_root = std::max(
                 0,
                 static_cast<int>(std::ceil(
                     (k + 1. - lti * world_size - p_i - p1 * ltk) / sqrtp1)));
             j_root < sqrtp1 &&
             l2g(X2p(p_i, j_root, ltk, p1), lti, world_size) < Nt;
             ++j_root) {
          recvgather[j_root] = v * nlayr;
        }
        //! merge reshuffled data along j-line
        MPI_Iallgatherv(
            MPI_IN_PLACE, 0, MPI_DOUBLE,
            &A10buffRcv[(lti * jk_size + ltk * sqrtp1) * v * nlayr],
            recvgather.data(), displs_j_vn.data(), MPI_DOUBLE, j_comm, &req_gat[lti * c + ltk]);
      }
    }

#ifdef TIME
    times[3] += std::chrono::high_resolution_clock::now() - start;
    start = std::chrono::high_resolution_clock::now();
#endif



    //!----------------------------------------------------------
    //! Writing results into final matrix
    //!----------------------------------------------------------

    //! diagonal A00 written by diagonal processor
    if (p_i == p_j && p_k == 0 && p_i == k % sqrtp1) {
      #pragma vector
      for (index_t i = 0; i < v; ++i) {
        #pragma vector
        for (index_t j = 0; j < v; ++j) {
          L[(((k / sqrtp1) * v + i) * tA11 + (k / sqrtp1)) * v + j] =
              A00buff[i * v + j];
        }
      }
    }

    const index_t lower_isend_bound = std::max(0, static_cast<int>(std::ceil((k + 1. - world_rank) / world_size)));
    //! subdiagonal column A10
    if (p_k == 0 && p_j == k % sqrtp1) {
      //! we write into L because we are on the front plane
      for (index_t lti = (k + sqrtp1 - p_i) / sqrtp1; lti < tA11; ++lti) {
        if (lti % jk_size == jk_rank) {
          //! write your stuff
          #pragma vector
          for (index_t i = 0; i < v; ++i) {
            #pragma vector
            for (index_t j = 0; j < v; ++j) {
              L[((lti * v + i) * tA11 + (k / sqrtp1)) * v + j] =
                  A10buff[((lti / jk_size) * v + i) * v + j];
            }
          }
        } else {
          //! receive your stuff and write
          req_vec.push_back(MPI_REQUEST_NULL);
          MPI_Irecv(&L[(lti * tA11 * v + (k / sqrtp1)) * v], 1, A11_tile_type,
                    lti % jk_size, lti % jk_size, jk_comm,
                    &req_vec[req_vec.size() - 1]);
        }
      }
    } else {
      //! we are not on the front, so we have to send
      for (index_t lti = lower_isend_bound;
           lti < tA10 && world_rank + lti * world_size < Nt; ++lti) {
        MPI_Isend(&A10buff[lti * v * v], v * v, MPI_DOUBLE, k % sqrtp1, jk_rank,
                  jk_comm, &req_arr[lti]);
      }
    }

#ifdef TIME
    times[4] += std::chrono::high_resolution_clock::now() - start;
    start = std::chrono::high_resolution_clock::now();
#endif


    //! end the iteration before data redistribution
    if (k == Nt - 1) break;

    //! Wait for packages
    MPI_Waitall(( tA10 - k/world_size) * c, req_gat + k / world_size * c, MPI_STATUSES_IGNORE);

    //! A01buffRcv is A10buffRcv but transposed in processor grid (when looking
    //! at one ij-plane)
    const index_t lower_sr_s = std::max(static_cast<int>(k / world_size) * jk_size, static_cast<int>(std::ceil((k + 1. - p_i) / sqrtp1))) * v * nlayr;
    const index_t lower_sr_r = std::max(static_cast<int>(k / world_size) * jk_size ,static_cast<int>(std::ceil((k + 1. - p_j) / sqrtp1))) * v * nlayr;
    if (p_i != p_j) {
      MPI_Sendrecv(&A10buffRcv[lower_sr_s], A10buffRcv.size() - lower_sr_s, MPI_DOUBLE,
                   X2p(p_j, p_i, 0, p1), world_rank % p1, 
                   &A01buffRcv[lower_sr_r], A01buffRcv.size() - lower_sr_r, MPI_DOUBLE, X2p(p_j, p_i, 0, p1),
                   X2p(p_j, p_i, 0, p1), ij_comm, &status);
      if(status.MPI_ERROR != 0) MPI_Abort(MPI_COMM_WORLD, status.MPI_ERROR);	
      if(status.MPI_SOURCE != X2p(p_j, p_i, 0, p1)) throw std::runtime_error("Wrong source in Sendrecv");
      if(status.MPI_TAG != X2p(p_j, p_i, 0, p1)) throw std::runtime_error("Wrong tag in Sendrecv");
      MPI_Get_count(&status, MPI_DOUBLE, &buff_size); 
      if(buff_size != A01buffRcv.size() - lower_sr_r) throw std::runtime_error("Data went missing in MPI_Sendrecv");
    } else {
      A01buffRcv = A10buffRcv;
    }

#ifdef TIME
    times[5] += std::chrono::high_resolution_clock::now() - start;
    start = std::chrono::high_resolution_clock::now();
#endif

    MPI_Barrier(world_comm);

    //!----------------------------------------------------------
    //! 2.5D matrix-matrix multiplication
    //!----------------------------------------------------------

    //! update the matrix to the right of the current column as preparative step
    //! for the next iteration the updated matrix gets smaller and smaller every
    //! iteration
    const index_t low_b = std::max(static_cast<int>(k / world_size), static_cast<int>(std::ceil((k + 1. - p_j) / sqrtp1))); 
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, (tA11 - low_b) * v, (tA11 - low_b) * v, nlayr, -1., &A10buffRcv[low_b * v * nlayr], nlayr, &A01buffRcv[low_b * v * nlayr], nlayr, 1., &A11buff[(low_b * tA11 * v + low_b) * v], tA11 * v);

#ifdef TIME
    times[6] += std::chrono::high_resolution_clock::now() - start;
    start = std::chrono::high_resolution_clock::now();
#endif

    MPI_Barrier(world_comm);

    //! Reduce the MMM results along the k-line (2.5D MMM)
    const index_t ltj = (k + 1) / sqrtp1;
    if (p_j == (k + 1) % sqrtp1) {
      index_t low =
          std::max(static_cast<int>(k / sqrtp1),
                   static_cast<int>(std::ceil((k + 1. - p_i)) / sqrtp1));
      //! manual packing for reduce to avoid writing own MPI_Op
      matrix_t redBuff((tA11 - low) * v * v, 0.);
      #pragma vector
      for (index_t i = 0; i < (tA11 - low) * v; ++i) {
        #pragma vector
        for (index_t j = 0; j < v; ++j) {
          redBuff[i * v + j] =
              A11buff[((low * v + i) * tA11 + ltj) * v + j];
        }
      }

      if (jk_rank / sqrtp1 == (k + 1) % c) {
        //! we are receiving the reduce, so we do in-place reduction...
        MPI_Reduce(MPI_IN_PLACE, redBuff.data(), (tA11 - low) * v * v,
                   MPI_DOUBLE, MPI_SUM, (k + 1) % c, k_comm);
        //! ... then write our buffer in the correct one
        #pragma vector
        for (index_t i = 0; i < (tA11 - low) * v; ++i) {
          #pragma vector
          for (index_t j = 0; j < v; ++j) {
            A11buff[((low * v + i) * tA11 + ltj) * v + j] =
                redBuff[i * v + j];
          }
        }
      } else {
        MPI_Reduce(redBuff.data(), redBuff.data(), (tA11 - low) * v * v,
                   MPI_DOUBLE, MPI_SUM, (k + 1) % c, k_comm);
      }
    }

#ifdef TIME
    times[7] += std::chrono::high_resolution_clock::now() - start;
    start = std::chrono::high_resolution_clock::now();
#endif

    //!----------------------------------------------------------
    //! Data redistribution
    //!----------------------------------------------------------

    //! Broadcast the next diagonal tile to everyone
    int root = X2p((k + 1) % sqrtp1, (k + 1) % sqrtp1, (k + 1) % c, p1);
    if (world_rank == root) {
      #pragma vector
      for (index_t i = 0; i < v; ++i) {
        #pragma vector
        for (index_t j = 0; j < v; ++j) {
          A00buff[i * v + j] =
              A11buff[(((k + 1 - p_i) / sqrtp1 * v + i) * tA11 +
                      (k + 1 - p_i) / sqrtp1) * v + j];
        }
      }
    }
    MPI_Ibcast(A00buff.data(), v * v, MPI_DOUBLE, root, world_comm, &reqA00);

#ifdef TIME
    times[8] += std::chrono::high_resolution_clock::now() - start;
    start = std::chrono::high_resolution_clock::now();
#endif

  
    MPI_Waitall(req_count, &req_arr[lower_isend_bound], statuses);
    for(int i = 0; i < req_count; ++i){
      if(statuses[i].MPI_ERROR != 0) throw std::runtime_error("Waitall failed with Errorcode " + std::to_string(statuses[i].MPI_ERROR));
    }
    req_count = 0;

     //! scatter the column below the next diagonal tile
    for (index_t lti = (k / sqrtp1) / jk_size; lti < tA10 && l2g(p_i, lti, world_size) < Nt; ++lti){ 
      std::fill(sendcounts.begin(), sendcounts.end(), 0);
      #pragma vector
      for (index_t i = std::max(0, static_cast<int>(k / sqrtp1 - lti * jk_size));
           i < jk_size && l2g(p_i, lti * jk_size + i, sqrtp1) < Nt; ++i) {
        sendcounts[i] = 1;
      }
      if (sendcounts[jk_rank] > 0 || jk_rank == root / sqrtp1) {
        MPI_Scatterv(&A11buff[lti * jk_size * tA11 * v * v + ltj * v], sendcounts.data(),
                     displs_jk.data(), A11_tile_type,
                     &A10buff[lti * v * v], v * v, MPI_DOUBLE,
                     root / sqrtp1, jk_comm);
      }
    }


#ifdef TIME
    times[9] += std::chrono::high_resolution_clock::now() - start;
    start = std::chrono::high_resolution_clock::now();
#endif
  } //! main loop end
  
  //!----------------------------------------------------------
  //! Cleanup
  //!----------------------------------------------------------
  
  MPI_Status stat_waitall[req_vec.size()];
  #pragma vector
  for(int p = 0; p < req_vec.size(); ++p){
    stat_waitall[p].MPI_ERROR = 0;
  }
  MPI_Waitall(req_vec.size(), req_vec.data(), stat_waitall); //!< wait for everything to be written

  for(int p = 0; p < req_vec.size(); ++p){
    if(stat_waitall[p].MPI_ERROR != 0) throw std::runtime_error("Waitall failed for rank " + std::to_string(p) + " with Errorcode " + std::to_string(stat_waitall[p].MPI_ERROR));
  }

  //! do some MPI cleanup
  MPI_Type_free(&A11_tile_type);
  MPI_Type_free(&A10_nlayr_type);

  MPI_Comm_free(&jk_comm);
  MPI_Comm_free(&ij_comm);
  MPI_Comm_free(&j_comm);
  MPI_Comm_free(&k_comm);

  MPI_Pcontrol(-1, "cholesky");

#ifdef TIME
  //! time absolut end; write files
  std::ofstream time_file;
  time_file.open("time" + std::to_string(world_rank) + ".txt");
  if (time_file.bad()) {
    throw std::runtime_error("couldn't open file");
  }
  time_file << "Timings for rank " << std::to_string(world_rank)
            << " of #ranks = " << std::to_string(world_size) << std::endl;
  std::chrono::duration<double> otime(0);
  for (auto x : times) {
    otime += x;
  }
  const auto overall_time = otime.count();
  time_file << std::setw(15) << "Initialization:" << std::setprecision(5)
            << std::setw(10) << times[0].count() << "s; that is "
            << times[0].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << std::setw(15) << "DPOTRF:" << std::setprecision(5)
            << std::setw(10) << times[1].count() << "s; that is "
            << times[1].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << std::setw(15) << "DTRSM:" << std::setprecision(5)
            << std::setw(10) << times[2].count() << "s; that is "
            << times[2].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << std::setw(15) << "Scat & Bcast:" << std::setprecision(5)
            << std::setw(10) << times[3].count() << "s; that is "
            << times[3].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << std::setw(15) << "Writing:" << std::setprecision(5)
            << std::setw(10) << times[5].count() << "s; that is "
            << times[4].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << std::setw(15) << "Sendrecv:" << std::setprecision(5)
            << std::setw(10) << times[4].count() << "s; that is "
            << times[5].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << std::setw(15) << "DGEMM:" << std::setprecision(5)
            << std::setw(10) << times[6].count() << "s; that is "
            << times[6].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << std::setw(15) << "Reduce:" << std::setprecision(5)
            << std::setw(10) << times[7].count() << "s; that is "
            << times[7].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << std::setw(15) << "A00 Bcast:" << std::setprecision(5)
            << std::setw(10) << times[8].count() << "s; that is "
            << times[8].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << std::setw(15) << "A11 Scat:" << std::setprecision(5)
            << std::setw(10) << times[9].count() << "s; that is "
            << times[9].count() / overall_time * 100 << "% of the function."
            << std::endl;
  time_file << "The whole function took " << std::setprecision(5)
            << overall_time << "s to complete." << std::endl;
  time_file.close();
#endif
}

//!
//! Distributes a matrix in block-cyclic manner.
//! \param A global matrix
//! \param A_buffer local reordered matrices - same size as a A
//! \param N dimension of the global matrix
//! \param v tile size
//! \param sqrtp1 size of the first/second dimension of the processor grid
//! \param p1 sqrtp1 * sqrtp1
//!
void distribute(const matrix_t& A, matrix_t& A_buffer, index_t N, index_t v,
                index_t sqrtp1, index_t p1) {
  const index_t Nt = std::ceil(1. * N / v);
  const index_t tA11 = std::ceil(1. * Nt / sqrtp1); 
  const index_t A_process_size = A_buffer.size() / p1;
  for (index_t p = 0; p < p1; ++p) {
    for (index_t lti = 0; lti < tA11; ++lti) {
      for (index_t i = 0; i < v; ++i) {
        for (index_t ltj = 0; ltj < tA11; ++ltj) {
          for (index_t j = 0; j < v; ++j) {
            A_buffer.at(p * A_process_size + lti * tA11 * v * v + i * tA11 * v +
                     ltj * v + j) =
                A.at(((p % sqrtp1) * v + lti * sqrtp1 * v + i) * N +
                  (p / sqrtp1) * v + ltj * sqrtp1 * v + j);
          }
        }
      }
    }
  }
}


//!
//! Assembles a matrix in block-cyclic manner. Reverse of distribute.
//! \param A global matrix
//! \param A_buffer local reordered matrices - same size as a A
//! \param N dimension of the global matrix
//! \param v tile size
//! \param sqrtp1 size of the first/second dimension of the processor grid
//! \param p1 sqrtp1 * sqrtp1
//!
void assemble(matrix_t& A, const matrix_t& A_buffer, index_t N, index_t v,
              index_t sqrtp1, index_t p1) {
  assert(A.size() == N * N);
  index_t Nt = std::ceil(1. * N / v);
  index_t tA11 = std::ceil(1. * Nt / sqrtp1);
  const index_t A_process_size = A_buffer.size() / p1;
  assert(tA11 * tA11 * v * v == A_process_size);
  for (int p = 0; p < p1; ++p) {
    for (index_t lti = 0; lti < tA11; ++lti) {
      for (index_t i = 0; i < v; ++i) {
        for (index_t ltj = 0; ltj < tA11; ++ltj) {
          for (index_t j = 0; j < v; ++j) {
            A[((p % sqrtp1) * v + lti * sqrtp1 * v + i) * N + (p / sqrtp1) * v +
              ltj * sqrtp1 * v + j] =
                A_buffer[p * A_process_size + lti * tA11 * v * v +
                         i * tA11 * v + ltj * v + j];
          }
        }
      }
    }
  }
}

//!
//! Performs several timed runs of the cholesky decomposition for given matrix
//! that is potentially upscaled to fit the tile size. To be used to check for
//! correctness.
//! \param inpA input matrix (global) - rank 0 must have the data
//! \param params parameters for decomposition as calculated by
//! calculateParameters
//! \param world_comm communicator in which the function
//! should run
//! \param runs number of runs to perform to get statistical data
//!
void distributed_cholesky(matrix_t& inpA,
                          std::tuple<index_t, index_t, index_t, index_t> params,
                          MPI_Comm world_comm, index_t runs) {
  const index_t inpN = std::sqrt(inpA.size());
  const index_t sqrtp1 = std::get<0>(params);
  const index_t c = std::get<1>(params);
  const index_t v = std::get<2>(params);
  const index_t N = std::get<3>(params);
  const index_t local_N = N / sqrtp1;
  const index_t p1 = sqrtp1 * sqrtp1;

  int rank;
  MPI_Comm_rank(world_comm, &rank);

  MPI_Status status;
  status.MPI_ERROR=0;
  int buff_size;


  matrix_t A_process(local_N * local_N, 0.);
  if (rank == 0) {
    matrix_t A_buffer(N * N, 0.);
    if (inpN != N) {
      matrix_t A(N * N, 0);
      for (index_t i = 0; i < inpN; ++i) {
        std::move(inpA.data() + i * inpN, inpA.data() + (i + 1) * inpN,
                  A.data() + i * N);
      }
      for (index_t i = inpN; i < N; ++i) {
        A[i * N + i] = 1.;
      }
      //! distribute data
      distribute(A, A_buffer, N, v, sqrtp1, p1);
    } else {
      distribute(inpA, A_buffer, N, v, sqrtp1, p1);
    }
    std::move(A_buffer.begin(), A_buffer.begin() + A_process.size(),
              A_process.begin());

    for (int r = 1; r < p1; ++r) {
      MPI_Send(&A_buffer[r * local_N * local_N], local_N * local_N, MPI_DOUBLE,
               r, r, world_comm);
    }
  } else if (rank < p1) {
    MPI_Recv(A_process.data(), local_N * local_N, MPI_DOUBLE, 0, rank,
             world_comm, &status);
    if(status.MPI_ERROR != 0) MPI_Abort(MPI_COMM_WORLD, status.MPI_ERROR);	
    if(status.MPI_SOURCE != 0) throw std::runtime_error("Wrong source in Recv");
    if(status.MPI_TAG != rank) throw std::runtime_error("Wrong tag in Recv");
    MPI_Get_count(&status, MPI_DOUBLE, &buff_size); 
    if(buff_size != local_N * local_N) throw std::runtime_error("Data went missing in MPI_Recv");
  }

  matrix_t A_process_copy = A_process;

  LSB_Rec_enable();

  //! non-working LibSciBench window synchronization
  //  for (index_t window_run = 0; window_run < 3; ++window_run) {
  //    A_process = A_process_copy;
  //    LSB_Res();
  //    cholesky(A_process, p1, c, local_N, N, v, world_comm);
  //    LSB_Rec(0);
  //  }
  //  double window;
  //  LSB_Fold(0, LSB_MAX, &window);
  //  LSB_Sync_reset(2. * window);

  for (index_t run = 0; run < runs; ++run) {
    A_process = A_process_copy;
    //    double err = LSB_Sync();
    //    LSB_Set_Rparam_double("sync_error", err);
    MPI_Barrier(world_comm);
    LSB_Res();
    //! actual cholesky decomposition
    cholesky(A_process, p1, c, local_N, N, v, world_comm);
    LSB_Rec(run);
  }
  LSB_Rec_disable();

  //! AVENGERS: ASSEMBLE the matrix and put the output in rank 0's buffer
  if (rank > 0 && rank < p1) {
    MPI_Send(A_process.data(), A_process.size(), MPI_DOUBLE, 0, rank,
             world_comm);
  } else if (rank == 0) {
    matrix_t A_buffer(p1 * A_process.size(), 0.);
    std::move(A_process.begin(), A_process.end(), A_buffer.begin());
    for (int r = 1; r < p1; ++r) {
      MPI_Recv(&A_buffer[r * A_process.size()], A_process.size(),
               MPI_DOUBLE, r, r, world_comm, &status);
      if(status.MPI_ERROR != 0) MPI_Abort(MPI_COMM_WORLD, status.MPI_ERROR);	
      if(status.MPI_SOURCE != r) throw std::runtime_error("Wrong source in Recv");
      if(status.MPI_TAG != r) throw std::runtime_error("Wrong tag in Recv");
      MPI_Get_count(&status, MPI_DOUBLE, &buff_size); 
      if(buff_size != local_N * local_N) throw std::runtime_error("Data went missing in MPI_Recv");
    }
    if (inpN != N) {
      matrix_t A(N * N, 0);
      assemble(A, A_buffer, N, v, sqrtp1, p1);
      if (inpN != N) {
        for (index_t i = 0; i < inpN; ++i) {
          std::move(A.data() + i * N, A.data() + i * N + (i + 1),
                    inpA.data() + i * inpN);
        }
      }
    } else {
      assemble(inpA, A_buffer, N, v, sqrtp1, p1);
    }
  }
}
