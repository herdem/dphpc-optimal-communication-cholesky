#include <liblsb.h>
#include <mkl.h>
#include <mpi.h>

#include <cmath>
#include <iomanip>
#include <iostream>
#include <random>
#include <stdexcept>
#include <tuple>
#include <vector>

#include "cholesky.hpp"

using index_t = std::size_t;

int main(int argc, char* argv[]) {
  MPI_Init(nullptr, nullptr);

  LSB_Init("Cholesky", 0);
  LSB_Rec_disable();

  constexpr unsigned int runs = 1;
  
  if (argc != 4) {
    return -1;
  }
  const index_t lower_N = std::atoi(argv[1]);
  const index_t upper_N = std::atoi(argv[2]);
  const index_t step_size = std::atoi(argv[3]);

  int rank, P;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &P);
  // if (rank == 0) {
  //  const PAPI_hw_info_t* hwinfo = nullptr;
  //  if ((hwinfo = PAPI_get_hardware_info()) == nullptr) {
  //    std::cerr << "Couldn't retrieve hardware info.\n";
  //    return -1;
  //  }
  //  auto space = std::setw(15);
  //  std::cout << space << "#CPUs:" << space << hwinfo->ncpu << std::endl;
  //  std::cout << space << "#nodes:" << space << hwinfo->nnodes << std::endl;
  //  std::cout << space << "#totalCPUs:" << space << hwinfo->totalcpus
  //            << std::endl;
  //  std::cout << space << "vendor:" << space << hwinfo->vendor << std::endl;
  //  std::cout << space << "vendor_string:" << space << hwinfo->vendor_string
  //            << std::endl;
  //  std::cout << space << "model:" << space << hwinfo->model << std::endl;
  //  std::cout << space << "model_string:" << space << hwinfo->model_string
  //            << std::endl;
  //  std::cout << space << "revision:" << space << hwinfo->revision <<
  //  std::endl; std::cout << space << "mhz:" << space << hwinfo->mhz <<
  //  std::endl;
  //}

  // Initialising spd matrix A
  for (index_t multiply = 1; multiply < 2; ++multiply) {
    for (std::size_t N = lower_N; N <= upper_N; N += step_size) {
      std::vector<double> A(N * N, 0.), A_temp(N * N, 0.);

      // create matrix
      if (rank == 0) {
        std::uniform_real_distribution<> dis(0.01, 10.);
        std::random_device rd;
        std::mt19937 gen(rd());
        double num = 0;
        for (std::size_t i = 0; i < N; ++i) {
          for (std::size_t j = 0; j < N; ++j) {
            num = dis(gen);
            A_temp[i * N + j] = num;
          }
        }

        // A = 0.5 * (A + A');
        for (std::size_t i = 0; i < N; ++i) {
          for (std::size_t j = 0; j < N; ++j) {
            A[i * N + j] = 0.5 * (A_temp[i * N + j] + A_temp[j * N + i]);
          }
        }

        // A = A + (n * eye(n));
        for (std::size_t i = 0; i < N; ++i) {
          A[i * N + i] += N;
        }
      //for (std::size_t i = 0; i < N; ++i) {
      //  for (index_t j = 0; j < N; ++j) {
      //      A_temp[i * N + j] = dis(gen);
      //  }
      //}
    
      //cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, N, N, N, 1., A_temp.data(), N, A_temp.data(), N, 0., A.data(), N);

	//for (std::size_t i = 0; i < N; ++i) {
	//	for (std::size_t j = 0; j < N; ++j) {
	//		if (i == j) A[i * N + j] = N * N;
	//		else A[i * N + j] = i + j;
	//	}
	//}

        A_temp = A;
      }

      // parameter tuning
      auto params = calculateParameters(N, P);
      const index_t sqrtp1 = std::get<0>(params);
      const index_t c = std::get<1>(params);
      const index_t v = std::get<2>(params);
      const index_t newP = sqrtp1 * sqrtp1 * c;

      // create world_comm
      MPI_Comm world_comm;
      MPI_Comm_split(MPI_COMM_WORLD, rank / newP, rank, &world_comm);

      LSB_Set_Rparam_int("N", N);
      LSB_Set_Rparam_int("tile_size", v);
      LSB_Set_Rparam_int("inpP", P);
      LSB_Set_Rparam_int("P", newP);
      LSB_Set_Rparam_int("p1", sqrtp1 * sqrtp1);
      LSB_Set_Rparam_int("c", c);

      //    double dummy = 2.;
      //    LSB_Sync_init(world_comm, dummy);

      if (rank < newP) {
        MPI_Barrier(world_comm);
        try {
          distributed_cholesky(A, params, world_comm, runs);
        } catch (std::exception& e) {
          std::cerr << "distributed_cholesky threw exception '" << e.what()
                    << "' from rank " << rank << std::endl;
          MPI_Abort(MPI_COMM_WORLD, -1);
        }
        MPI_Barrier(world_comm);

        // sanity check
        if (rank == 0) {
          int error =
              LAPACKE_dpotrf(LAPACK_ROW_MAJOR, 'L', N, A_temp.data(), N);
          if (error != 0) {
            std::cerr << "serial dpotrf failed!" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, -1);
          }
	  double frob_norm = 0.;
          for (std::size_t i = 0; i < N; ++i) {
            for (std::size_t j = 0; j <= i; ++j) {
              //if (std::abs(A_temp[i * N + j] - A[i * N + j]) > 1e-10) {
              //  std::cerr << A_temp[i * N + j] << " != " << A[i * N + j]
              //            << "\n";
              //  throw std::runtime_error("wrong in " + std::to_string(i) +
              //                           ", " + std::to_string(j));
              frob_norm += std::pow(A_temp[i * N + j] - A[i * N + j], 2);
              //}
            }
          }
      	std::cout << "Frobenius norm: " << std::sqrt(frob_norm) << std::endl;
	}
      }
      MPI_Comm_free(&world_comm);
      MPI_Barrier(MPI_COMM_WORLD);
    }
  }

  LSB_Finalize();
  MPI_Finalize();
}
