#include <mkl.h>
#include <liblsb.h>

#include <iostream>
#include <iomanip>
#include <vector>

int main() {
  LSB_Init("Cholesky_MKL_sequential", 0);
  
  constexpr unsigned int runs = 10;
  
//  const PAPI_hw_info_t* hwinfo = nullptr;
//  if ((hwinfo = PAPI_get_hardware_info()) == nullptr) {
//    std::cerr << "Couldn't retrieve hardware info.\n";
//    return -1;
//  }
//  auto space = std::setw(15);
//  std::cout << space << "#CPUs:" << space << hwinfo->ncpu << std::endl;
//  std::cout << space << "#nodes:" << space << hwinfo->nnodes << std::endl;
//  std::cout << space << "#totalCPUs:" << space << hwinfo->totalcpus
//  << std::endl;
//  std::cout << space << "vendor:" << space << hwinfo->vendor << std::endl;
//  std::cout << space << "vendor_string:" << space << hwinfo->vendor_string
//  << std::endl;
//  std::cout << space << "model:" << space << hwinfo->model << std::endl;
//  std::cout << space << "model_string:" << space << hwinfo->model_string
//  << std::endl;
//  std::cout << space << "revision:" << space << hwinfo->revision << std::endl;
//  std::cout << space << "mhz:" << space << hwinfo->mhz << std::endl;
  
  constexpr unsigned int N = 25000;
  std::vector<double> A(N * N), A_copy(N * N);
  for (unsigned int i = 0; i < N; ++i) {
    for (unsigned int j = 0; j < N; ++j) {
      if (i == j) {
        A[i * N + j] = N * N;
      } else {
        A[i * N + j] = i + j;
      }
    }
  }
  
  for (unsigned int run = 0; run < runs; ++run) {
    A = A_copy;
    LSB_Res();
    int error = LAPACKE_dpotrf(LAPACK_ROW_MAJOR, 'L', N, A.data(), N);
    LSB_Rec(run);
    if (error != 0) {
      return -1;
    }
  }
  
  LSB_Finalize();
}
