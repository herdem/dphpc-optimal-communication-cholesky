# Questions
## Optimizations
[ ] How can you see where the speed bottleneck in our program is?

[ ] How do we integrate MKL into CMake?
* Write on Thursday concerning Scripts

[ x - use MPI_Datatype ] Is it better to write into temporary send buffer, then send once or to do send for every row when sending a block of a matrix?
* It depends
* Paper: Use MPI_DATATYPES, manually packing could be better in practice. (pointer to buffer and do some offsetting. If code fast enough, fastest version available)
* Manual Packing big overhead
* A10 and A11: in theory measure communication volume smaller, in practice performance trade offs. Communication optimal in python code. For fast cholesky can be discussed.
* Open problem: Depending on implementation, might be better to only use 2 ranks and not use (A10?) at all. Advantage: Use BC and Scatter
* https://spcl.inf.ethz.ch/Publications/index.php?pub=167

[ ] We could potentially replace a lot of sends and receives with scatters/gathers etc. when we create new communicators in every k iteration? Is Send / Receive faster or the communicatior creation approach?
* Rank mapping doesn't change but gti is filtered. 
* Tiles are not physical entities but a logical mappings.
* A10 scattering: call scatter on the same buffer but point at a different starting offset
* Design communicator apropriately. Take all the ranks with same pi but different pk and pj. Look At A10 to A11 mapping.

## Metrics and Paper
[ ] What are matrix sizes that we should benchmark?
* Smallest size: 4000 x 4000
* Reasonable: 10000 x 10000 - 40000 x 40000

[ ] How many processors?
* Scaling
* 4000 x 4000 with 16 compute node. Hundreds of cores -> Unreasonable
* Perfect scaling: Scaled well with extreme number of ranks for small date
* Experiment: Smallest matrix such that still reasonable

[ ] How do you measure communication?
* Not easy for closed source libraries
* Scorpe?
* BLL, static library, intercept low level operations
* Assume in practice (high prob): For LU optimized libs (MKL) matches lower bound for 2D decomposition (M^2/sqrt(P))
* Introduce own counters to measure communication (collective communication should be calculated) N^2/(2p^(2/3))
* How do you measure network traffic?
O(n^2/([2]p^(2/3))) for cholesky

[ ] Also, how do you measure performance (e.g. in GFLOP/s)?
* Time: Measure MPI time
* gflops: Number of flops (N^2/3 fma)
* dtrsm (N^2/2)
* Lookup gflops and bounds

[ ] To which libraries should we benchmark our performance to? Scalapack...
* Scalapack vs Our: Our 3D A11 3d decomposed
  Cut columns and distribute to different layers
* MKL is fastest you can get
* Distributed code
* Scalapack script

[ ] Speedup?
* Expect very large matrices, dominated by flopper. For small matrices (ratio size / p) dominated by communication
* Large: At least 50 - 80% peak performance (check flops)
* Small: 
Minimal requirement does communication scale as it should (correct communication) 
Ambition: LU with pivoting even for small matrices get 70 or 80% of peak performance
24 compute nodes dual socket e5, run it on 10000x10000 and peak flops 60% or 70%
* Might be easier for cholesky since static
* Trivia: LU decomposition as metric for ranking of top 500 hpc
* Puppy counters: Lib. Instrument your code. Information about hardware counter (check link

[ ] Get peak performance?
* Measure required flops and divide by time
* Compare flops achieved and core flops by num processors

## Outlook
[ ] What could be done in terms of GPUs? Just replacing the MKL-BLAS calls with CUDA-BLAS calls?
* Yes, but naive speedup smaller
* Unreasonable to do tile by tile because of overhead
* Designing fast gpu code: Minimize transfers gpu and cpu
* Keep data on gpu and communicate as little as possible
* Change layout, etc...

