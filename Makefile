# CPPFLAGS += -Wall -Wextra
CPPFLAGS += -std=c++17 -O3 -DNDEBUG #-DTIME
CPPFLAGS += -m64 -march=skylake-avx512 
CPPFLAGS += -I${MKLROOT}/include -I${HOME}/liblsb

cholesky.linux: Cholesky/main.cpp Cholesky/cholesky.cpp
	mpiicpc $^ ${CPPFLAGS} -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group ${HOME}/liblsb/.libs/liblsb.a -lpthread -lm -ldl -o build/$@

scalapack.linux: Scalapack/scalapack.cpp Scalapack/func_scal.cpp
	mpiicpc $^ ${CPPFLAGS} ${MKLROOT}/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_blacs_intelmpi_lp64.a  -Wl,--end-group ${HOME}/liblsb/.libs/liblsb.a -lpthread -lm -ldl -o build/$@

cholesky.mac: Cholesky/main.cpp Cholesky/cholesky.cpp
	mpicxx $^ ${CPPFLAGS} ${MKLROOT}/lib/libmkl_intel_lp64.a ${MKLROOT}/lib/libmkl_sequential.a ${MKLROOT}/lib/libmkl_core.a -llsb -lpthread -lm -ldl -o build/$@

.PHONY: clean
clean: 
	${RM} *.o *.out
